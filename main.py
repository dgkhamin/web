from flask import Flask, render_template, request, redirect, url_for

app = Flask(__name__)

@app.route("/")
def home():
    return render_template("index.html")

@app.route("/wheel")
def wheel():
    return render_template("wheel.html")

@app.route('/admin/<usr>')
def admin(usr):
    return render_template('for_admin.html',usr = usr)

@app.route('/aboutadmin')
def Contact_admin():
    return render_template('contactadmin.html')

@app.route('/talktoadmin')
def talk_to_admin():
    return render_template('talk2admin.html')

@app.route('/sendData')
def send_ms_to_admin():
    fname = request.args.get('fname')
    description = request.args.get('description')
    return render_template('goturms.html', data={"name":fname,"description":description})

@app.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'POST':
        username = request.form['username']
        password = request.form['password']
        # ตรวจสอบข้อมูลเข้าสู่ระบบ (ในตัวอย่างนี้จะเป็นตรวจสอบชื่อผู้ใช้และรหัสผ่านแบบจำลอง)
        if username == 'admin' and password == 'password':
            # หากข้อมูลถูกต้องให้เข้าสู่ระบบได้
            return redirect(url_for('success', username=username))
        else:
            # หากข้อมูลไม่ถูกต้องให้แสดงหน้า login อีกครั้ง
            return render_template('index.html', error='Invalid username or password')
    return render_template('register.html')

@app.route('/singin')
def signin():
    return render_template('singin.html')

@app.route('/success/<username>')
def success(username):
    return f'<h2>Welcome back, {username}!</h2>'

@app.route('/sorry')
def sorry():
    return render_template('sorry.html')

if __name__ == "__main__":
    app.run(debug=True)
